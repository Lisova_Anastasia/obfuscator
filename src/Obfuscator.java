import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Обфускатор для
 * приведения кода программы к виду,
 * сохраняющему её функциональность,
 * но затрудняющему анализ и
 * понимание алгоритмов работы
 *
 * @author Лисова Анастасия, 17ИТ17
 */
class Obfuscator {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        String path = getFilePath();
        if (!checkPath(path)) {
            System.out.println("Файл не найден!");
            return;
        }
        String program;
        program = readFile(path);
        program = deleteONLineComments(program);
        program = deleteMultilineComments(program);
        program = deleteTransitionSymbols(program);
        program = changeVariablesName(program);
        program = changeMethodName(program);
        program = changeClassAndConstructorsName(path, program);
        File newCodeFile = new File(getNewFilePath());
        writeFile(newCodeFile, program);
        changeFileName(newCodeFile);
    }

    /**
     * Метод изменяет имя файла
     *
     * @param newCodeFile файл
     */
    private static void changeFileName(File newCodeFile) {
        newCodeFile.renameTo(new File(newCodeFile.getParent() + "\\" + "Z.java"));
    }

    /**
     * Возвращает строку программы
     * с изменёнными именами класса
     * и конструкторов
     *
     * @param path путь к исходной программе
     * @param program строка программы
     * @return изменённую строку программы
     */
    private static String changeClassAndConstructorsName(String path, String program) {
        File initialCodeFile = new File(path);
        String className = initialCodeFile.getName().replace(".java", "");
        program = program.replaceAll(className, "Z");
        return program;
    }

    /**
     * Возвращает строку программы
     * с изменёнными именами методов
     *
     * @param program строка программы
     * @return изменённую строку программы
     */
    private static String changeMethodName(String program) {
        Pattern pattern = Pattern.compile("(?<=\\w\\s)(?!main)[^A-Z]\\w*(?=\\()");
        Matcher myMatcher = pattern.matcher(program);
        String txt;
        for (char letterFirst = 'a'; letterFirst <= 'z'; letterFirst++) {
            for (char letterSecond = 'a'; letterSecond <= 'z'; letterSecond++) {
                if (myMatcher.find()) {
                    txt = myMatcher.group();
                    program = program.replaceAll(txt, String.valueOf(letterFirst) + (letterSecond));
                }
            }
        }
        return program;
    }

    /**
     * Возвращает строку программы
     * с изменёнными именами переменных
     *
     * @param program строка программы
     * @return изменённую строку программы
     */
    private static String changeVariablesName(String program) {
        Pattern pattern = Pattern.compile("(?<=\\w\\s|] |> )\\w\\w+(?=\\s*[=;])");
        Matcher myMatcher = pattern.matcher(program);
        String txt;
        for (char letterFirst = 'a'; letterFirst <= 'z'; letterFirst++) {
            for (char letterSecond = 'a'; letterSecond <= 'z'; letterSecond++) {
                if (myMatcher.find()) {
                    txt = "(?<!\\w)" + myMatcher.group() + "(?!\\w)";
                    program = program.replaceAll(txt, String.valueOf(letterFirst) + (letterSecond));
                }
            }
        }
        return program;
    }

    /**
     * Возвращает строку программы
     * с удалёнными однострочными
     * комментариями
     *
     * @param program строка программмы
     * @return изменённую строку программы
     */
    private static String deleteONLineComments(String program) {
        return program.replaceAll("/{2,}(.+?)[^\n]+", "");
    }

    /**
     * Возвращает строку программы
     * с удалёнными многострочными
     * комментариями
     *
     * @param program строка программы
     * @return изменённую строку программы
     */
    private static String deleteMultilineComments(String program) {
        return program.replaceAll("/\\*(\\*)*[^/]+\\w*\\*/", "");
    }

    /**
     * Возвращает строку программы
     * с удалёнными лишними знаками
     * разделения
     *
     * @param program строка программы
     * @return изменённую строку программы
     */
    private static String deleteTransitionSymbols(String program) {
        program = program.replaceAll("\\s+", " ");
        return program;
    }

    /**
     * Возвращает строку пути к файлу,
     * которую ввёл пользователь
     *
     * @return строку пути к файлу
     */
    private static String getFilePath() {
        System.out.print("Введите абсолютный путь к файлу с расширением .java: ");
        return scanner.nextLine();
    }

    /**
     * Возвращает строку пути к файлу,
     * в котором будет записываться
     * изменённая программа
     *
     * @return строку пути к файлу
     */
    private static String getNewFilePath() {
        System.out.print("Введите путь к файлу, в который хотите записать изменённый код программы: ");
        return scanner.nextLine();
    }

    /**
     * Возвращает true, если путь
     * соответствует регулярному
     * выражению, иначе false
     *
     * @param path путь
     * @return true, если верный путь
     */
    private static boolean checkPath(String path) {
        return path.matches("[A-Z]:(\\\\[a-zA-Z]+)+\\\\?(.java)");
    }

    /**
     * Возвращает строку, содержащую
     * текст из прочитанного файла
     *
     * @return строку
     */
    private static String readFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    /**
     * Записывает отредактированную
     * программу в файл newCodeFile
     *
     * @param program программа
     */
    private static void writeFile(File newCodeFile, String program) throws IOException {
        Files.write(Paths.get(String.valueOf(newCodeFile)), Collections.singleton(program));
    }
}